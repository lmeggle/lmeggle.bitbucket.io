var namespaces_dup =
[
    [ "class_list", "namespaceclass__list.html", null ],
    [ "Controller", "namespaceController.html", null ],
    [ "Encoder", "namespaceEncoder.html", null ],
    [ "IMUDriver", "namespaceIMUDriver.html", null ],
    [ "main", "namespacemain.html", null ],
    [ "main_step", "namespacemain__step.html", null ],
    [ "mainpage", "namespacemainpage.html", null ],
    [ "MotorDriver", "namespaceMotorDriver.html", null ],
    [ "PID", "namespacePID.html", null ],
    [ "project_page", "namespaceproject__page.html", null ],
    [ "Receiver", "namespaceReceiver.html", null ],
    [ "tuning_page", "namespacetuning__page.html", null ]
];