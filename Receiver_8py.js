var Receiver_8py =
[
    [ "Receiver", "classReceiver_1_1Receiver.html", "classReceiver_1_1Receiver" ],
    [ "ch_mode", "Receiver_8py.html#a948a9cec6603e956090637159dd3b98d", null ],
    [ "ch_pitch", "Receiver_8py.html#a8f3673bfd44ae76af14dce105dda96ff", null ],
    [ "ch_roll", "Receiver_8py.html#a31f36ebf60796cc48c1f62907bb2c433", null ],
    [ "mode", "Receiver_8py.html#a3e84df211c94da0e51452011553e47a6", null ],
    [ "pinA8", "Receiver_8py.html#a695efd50cc296e30b8f4b2d786c33f6c", null ],
    [ "pinA9", "Receiver_8py.html#a9b6757efbbe21d3c7df0edc61879f6a7", null ],
    [ "pinB10", "Receiver_8py.html#a3b2ef18a34ea9e2f30dc830762b7ed11", null ],
    [ "pitch", "Receiver_8py.html#abb9d7dff4047fef9b4f26bbbf3f2a5d8", null ],
    [ "roll", "Receiver_8py.html#ac57d1cb25dd2b710fdddc589397b87dd", null ],
    [ "tim1", "Receiver_8py.html#a544f33e87c3bb2a717a21e81b97acaa3", null ],
    [ "tim2", "Receiver_8py.html#a1ce1da6f847d7dd1deb11c29f2d8842a", null ]
];