var page_classes =
[
    [ "Motor Driver", "page_MotorDriver.html", [
      [ "Example usage", "page_MotorDriver.html#sec_Motor_Driver_ex", null ],
      [ "Testing", "page_MotorDriver.html#sec_Motor_Driver_test", null ]
    ] ],
    [ "Encoder", "page_Encoder.html", [
      [ "Example usage", "page_Encoder.html#sec_Encoder_ex", null ]
    ] ],
    [ "Controller", "page_Controller.html", [
      [ "Example usage", "page_Controller.html#sec_Controller_ex", null ],
      [ "Testing", "page_Controller.html#sec_Encoder_test", null ]
    ] ],
    [ "IMU Sensor", "page_IMU.html", [
      [ "Introduction", "page_IMU.html#sec_intro", null ],
      [ "Connection", "page_IMU.html#sec_first", null ],
      [ "Implementation of the class i2c", "page_IMU.html#sec_second", null ],
      [ "Demonstration of the sensor", "page_IMU.html#sec_final", null ],
      [ "Example usage", "page_IMU.html#sec_IMU_ex", null ]
    ] ],
    [ "PID Controller", "page_PID.html", [
      [ "Example usage", "page_PID.html#sec_PID_ex", null ],
      [ "Testing", "page_PID.html#sec_PID_test", null ]
    ] ],
    [ "Reciever PWM reading", "page_Receiver.html", [
      [ "Example usage", "page_Receiver.html#sec_Receiver_ex", null ],
      [ "Testing", "page_Receiver.html#sec_Receiver_test", null ]
    ] ]
];