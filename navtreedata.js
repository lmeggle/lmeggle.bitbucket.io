/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME405 Project", "index.html", [
    [ "Lab assignments", "index.html#sec_labs", null ],
    [ "Final Project", "index.html#sec_project", null ],
    [ "Class List", "page_classes.html", "page_classes" ],
    [ "Final Project: Flight Code", "page_Main.html", [
      [ "Introduction", "page_Main.html#sec_Main_script_intro", null ],
      [ "Initializing", "page_Main.html#sec_Main_script_init", null ],
      [ "Receiving Signals", "page_Main.html#sec_Main_script_receiving", null ],
      [ "Calculating the aileron position", "page_Main.html#sec_Main_script_calculating", null ],
      [ "Limiting the aileron position", "page_Main.html#sec_Main_script_limiting", null ],
      [ "Setting the Ailerons", "page_Main.html#sec_Main_script_setting", null ]
    ] ],
    [ "Final Term Project", "project_page.html", [
      [ "The Plan: A Flight Stabilization System", "project_page.html#sec_intropro", null ],
      [ "Parts List", "project_page.html#sec_firstpro", null ],
      [ "Timeline", "project_page.html#sec_secondpro", null ],
      [ "Safety Assessment", "project_page.html#sec_finalpro", null ],
      [ "Construction of the airplane", "project_page.html#sec_construction", null ],
      [ "Programming", "project_page.html#sec_programming", null ],
      [ "User Interface and Demonstration", "project_page.html#sec_interface", null ],
      [ "The Problems", "project_page.html#sec_problems", null ],
      [ "Testing", "project_page.html#sec_testing", null ]
    ] ],
    [ "Tuning of the DC-Motor Positional Control", "tuning_page.html", [
      [ "Introduction", "tuning_page.html#sec_introtuning", null ],
      [ "First run", "tuning_page.html#sec_firsttuning", null ],
      [ "Trying different values for kp", "tuning_page.html#sec_secondtuning", null ],
      [ "Finding the optimal gain", "tuning_page.html#sec_finaltuning", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Controller_8py.html",
"page_IMU.html#sec_intro"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';